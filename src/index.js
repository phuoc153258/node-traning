const express = require('express')
const fs = require("fs");
const path = require('path');
const route = require('./router/index.router')
const bodyParser = require('body-parser')

const app = express()
const port = 3000

app.use( express.static(__dirname + '/uploads'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

route(app)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})