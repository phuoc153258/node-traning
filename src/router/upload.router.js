const express = require('express');
const router = express.Router();
const uploadMiddleware = require('../middleware/upload.middleware')
const uploadController = require('../controller/upload.controller')

router.post('/single', uploadMiddleware.uploadSingle, uploadController.uploadSingle)

router.post('/multiple', uploadMiddleware.uploadMultiple, uploadController.uploadMultiple)

module.exports = router;