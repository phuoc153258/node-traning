const express = require('express');
const router = express.Router();
const uploadMiddleware = require('../middleware/upload.middleware')
const userMiddleware = require('../middleware/user.middleware')
const blogMiddleware = require('../middleware/blog.middleware')
const baseMiddleware = require('../middleware/base.middleware')
const userController = require('../controller/user.controller')
const blogController = require('../controller/blog.controller');

// Create blog with authorId
router.post('/:id/blog-items', uploadMiddleware.uploadFileBlog, blogMiddleware.createBlogCondition(), baseMiddleware.runConditionMiddleware, blogController.createBlog)

// Update user with id
router.put('/:id', uploadMiddleware.uploadSingle, userMiddleware.updateUser(), baseMiddleware.runConditionMiddleware, userController.updateUser)

// Delete user with id
router.delete('/:id', userMiddleware.deleteUser(), baseMiddleware.runConditionMiddleware, userController.deleteUser)

// Create user  
router.post('/', uploadMiddleware.uploadSingle, userMiddleware.createUser(), baseMiddleware.runConditionMiddleware, userController.createUser)

// Get details user
router.get('/:id', userMiddleware.getDetailsUser(), baseMiddleware.runConditionMiddleware, userController.getDetailsUser)

// Get all users search sort paginate
router.get('/', baseMiddleware.getAllUserCondition(), baseMiddleware.runConditionMiddleware, baseMiddleware.validateValuePaginateMiddleware, userController.getUser)

module.exports = router;