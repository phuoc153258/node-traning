const userRouter = require('./users.router')
const blogRouter = require('./blogs.router')
const uploadRouter = require('./upload.router')

function route(app) {

    app.use('/api/users', userRouter)

    app.use('/api/blogs', blogRouter)

    app.use('/api/uploads', uploadRouter)

    app.get('/', (req, res) => {
        res.send('Hello World!')
    })
}
module.exports = route;