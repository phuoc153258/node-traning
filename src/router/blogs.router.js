const express = require('express');
const router = express.Router();
const uploadMiddleware = require('../middleware/upload.middleware')
const blogMiddleware = require('../middleware/blog.middleware')
const baseMiddleware = require('../middleware/base.middleware')
const blogController = require('../controller/blog.controller');

// Update blog with blogId and authorId
router.put('/:blogId/users/:userId', uploadMiddleware.uploadFileBlog, blogMiddleware.updateBlogCondition(), baseMiddleware.runConditionMiddleware, blogController.updateBlog)

// Delete blog with blogId and authorId
router.delete('/:blogId/users/:userId', blogMiddleware.deleteBlogCondition(), baseMiddleware.runConditionMiddleware, blogController.deleteBlog)

// Get details blog with blogId and authorId
router.get('/:blogId/users/:userId', blogMiddleware.getDetailsBlogCondition(), baseMiddleware.runConditionMiddleware, blogController.getDetailsBlog)

// Get all blogs with authorId
router.get('/', baseMiddleware.getAllBlogWithAuthorIdCondition(), baseMiddleware.runConditionMiddleware, baseMiddleware.validateValuePaginateMiddleware, blogController.getAllBlogWithAuthorId)

module.exports = router;