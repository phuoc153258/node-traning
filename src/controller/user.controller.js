const users = require('../service/users.service')

module.exports = {
    getUser: function (req,res) {
        res.send(users.getUser(req))
    },
    getDetailsUser: function (req,res) {
        let result = users.getDetailsUser(req)
        if(!result) return res.send('Users is not exits !!!')
        res.send(result)
    },
    createUser: function(req,res) {
        // if (req.fileValidationError) 
        //     return res.send({code: 415, name: 'Unsupported Media Type !!!'});
        // if (!req.file) 
        //    return res.send('Please select an file to upload !!!');
        res.send(users.createUser(req))
    },
    updateUser: function (req, res) {
        res.send(users.updateUser(req))
    },
    deleteUser: function (req, res) {
        res.send(users.deleteUser(req))
    }
};