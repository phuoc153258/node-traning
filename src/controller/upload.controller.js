
module.exports = {
    uploadSingle: function (req, res) {
        if (req.fileValidationError)
            return res.send({ code: 415, name: 'Unsupported Media Type !!!' });
        if (!req.file)
            return res.send('Please select an file to upload !!!');
        res.send(req.file)
    },
    uploadMultiple: function (req, res) {
        res.send(req.files)
    }
};
