const blogs = require('../service/blogs.service')

module.exports = {
    getAllBlogWithAuthorId: function (req,res) {
        let result = blogs.getBlogByAuthorId(req)
        res.send(result)
    },
    getDetailsBlog: function (req,res) {
        res.send(blogs.getDetailsBlog(req))
    },
    createBlog: function (req,res) {
        res.send(blogs.createBlog(req))
    },
    updateBlog: function (req,res) {
        // if (req.fileValidationError) 
        //     return res.send({code: 415, name: 'Unsupported Media Type !!!'});
        // if (!req.files) 
        //     return res.send('Please select an file to upload !!!');
        res.send(blogs.updateBlog(req))
    },
    deleteBlog: function (req,res) {
        res.send(blogs.deleteBlog(req))
    },
    paginate: function (req,res) {
        let result = blogs.paginateBlog(req)
        res.send(result)
    },
    searchBlog: function (req,res) {
        let result = blogs.searchBlog(req)
        res.send(result)
    },
    sortBlog: function (req,res) {
        let result = blogs.sortBlog(req)
        res.send(result)
    }
};