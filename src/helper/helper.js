const { readFileSync, writeFileSync, appendFileSync } = require("fs");

const readFile = (path) => {
    return JSON.parse(readFileSync(path))
}

const writeFile = (path, data) => {
    writeFileSync(path, data);
}

const appendFile = (path, data) => {
    appendFileSync(path, JSON.stringify(data), {flag: 'a'});
}

const setTimeZone = (date, timeZone) => {
    date.setTime(date.getTime() + timeZone * 60 * 60 * 1000)
    return date.toISOString()
}

const configPathUpload = (path) => {
    return path.split('\\').slice(2).join('/')
}

module.exports = { readFile, writeFile, appendFile, setTimeZone, configPathUpload}