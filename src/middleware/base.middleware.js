const { query, validationResult } = require("express-validator");

const runConditionMiddleware = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()) return res.status(400).json({ errors: errors.array() });
    next();
};

const getAllBlogWithAuthorIdCondition = () => {
    return [
        query('authorId')
            .isString().notEmpty().isUUID().withMessage('Id must be string, not empty and format UUID !!!'),
        ...searchCondition(),
        ...sortBlogsCondition(),
        ...paginateCondition(),
    ]
}

const getAllUserCondition = () => {
    return [
        ...searchCondition(),
        ...sortUsersCondition(),
        ...paginateCondition()
    ]
}

const searchCondition = () => {
    return [
        query('search_value')
            .isString().withMessage('Search value must be string !!!')
    ]
}

const sortBlogsCondition = () => {
    return [
        query('field')
            .isString().isIn(['count_view', 'createdAt', '']).withMessage("Field must be string and must be count_view or createdAt or '' !!!"),
        query('type')
            .isString().isIn(['asc', 'desc', '']).withMessage("Type must be string and must be asc or desc or '' !!!"),
    ]
}

const sortUsersCondition = () => {
    return [
        query('field').isString().isIn(['age', 'createdAt', '']).withMessage("Field must be string and must be age or createdAt or '' !!!"),
        query('type').isString().isIn(['asc', 'desc', '']).withMessage("Type must be string and must be asc or desc or '' !!!"),
    ]
}

const paginateCondition = () => {
    return [
        query('limit')
            .toInt().isInt().withMessage('Limit must be number !!!'),
        query('page')
            .toInt().isInt().withMessage('Page must be number !!!'),
    ]
}

const validateValuePaginateMiddleware = (req, res, next) => {
    let { limit, page } = req.query;
    if (isNaN(limit) || limit <= 0) req.query.limit = 5;
    if (isNaN(page) || page <= 0) req.query.page = 1;
    next();
};

module.exports = {
    runConditionMiddleware, validateValuePaginateMiddleware, getAllBlogWithAuthorIdCondition, getAllUserCondition
};
