const { body, param } = require('express-validator');

const getDetailsBlogCondition = () => {
    return [
        ...deleteBlogCondition()
    ]
}

const createBlogCondition = () => {
    return [
        ...validateInfoBlogCondition(),
        ...validateIdCondition()
    ]
}

const updateBlogCondition = () => {
    return [
        body('count_view')
            .toInt().isInt().notEmpty().withMessage("Count View must be number and not empty !!!"),
        ...validateInfoBlogCondition(),
        ...validateBlogIdAndUserIdCondition()
    ]
}

const deleteBlogCondition = () => {
    return [
        ...validateBlogIdAndUserIdCondition()
    ]
}

const validateInfoBlogCondition = () => {
    return [
        body('title')
            .isString().notEmpty().isLength({max: 500}).withMessage("Title must be string, not empty and max length 500 !!!"),
        body('subTitle')
            .isString().notEmpty().isLength({max:5000}).withMessage("Sub Title must be string, not empty and max length 5000 !!!"),
        body('content')
            .isString().notEmpty().isLength({max:10000}).withMessage("Content must be string, not empty and max length 10000 !!!"),
        body('isPublic')
            .toBoolean().isBoolean().notEmpty().withMessage("Is public must be boolean and not empty !!!"),
    ]
}

const validateIdCondition = () => {
    return [
        param('id')
            .isString().notEmpty().isUUID().withMessage('Id must be string, not empty and format UUID !!!')
    ]
}

const validateBlogIdAndUserIdCondition = () => {
    return [
        param('blogId')
            .isString().notEmpty().isUUID().withMessage('Blog id must be string, not empty and format UUID !!!'),
        param('userId')
            .isString().notEmpty().isUUID().withMessage('User id must be string, not empty and format UUID !!!')
    ]
}

module.exports = {
    updateBlogCondition, createBlogCondition, deleteBlogCondition, getDetailsBlogCondition
}