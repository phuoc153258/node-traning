const { body, param } = require('express-validator');

const getDetailsUser = () => {
    return [
        ...idUserCondition()
    ]
}

const createUser = () => {
    return [
        body('name')
            .isString().notEmpty().isLength({max: 100}).withMessage("Name must be string, not empty and max length 100 !!!"),
        body('age')
            .toInt().isInt({min:1,max:200}).notEmpty().withMessage('Age must be number, range from 1 to 200 and not empty !!!'),
        body('email')
            .isString().isEmail().notEmpty().withMessage('Email must be string, format email and not empty !!!')
    ]
}

const updateUser = () => {
    return [
        ...createUser(),
        ...idUserCondition()
    ]
}

const deleteUser = () => {
    return [
        ...idUserCondition()
    ]
}

const idUserCondition = () => {
    return [
        param('id')
            .isString().notEmpty().isUUID().withMessage('Id must be string, not empty and format UUID !!!')
    ]
}

module.exports = {
    createUser, updateUser, deleteUser, getDetailsUser
}