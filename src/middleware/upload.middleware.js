const multer = require('multer')
const fs = require('fs');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        let date = new Date()
        let folderName = `./src/uploads/${file.mimetype.split('/')[0]}/${date.getDate()}-${date.getMonth()}-${date.getFullYear()}/`
        fs.mkdir(folderName, { recursive: true }, function(err) {
            if (err) cb(new Error(err), false)
            cb(null, folderName)
        })
    },
    filename: function(req, file, cb) {
        let filename = `${new Date().toISOString().replace(/:/g, '-')}-${file.originalname}`
        cb(null, filename)
    }
});

// const fileFilter = function(req, file, cb) {
//     const regex = new RegExp(`({png|jpg|jpeg|gif|svg|pdf|mp4|mov|avi})$`);
//     //  file.originalname.toLowerCase().match(/\.(png|jpg|jpeg|gif|svg|pdf|mp4|mov|avi)$/)
//     if ( regex.test(file.originalname.toLowerCase())) {
//         cb(null, true);
//     }
//     else {
//         cb(null,false)
//     }
// };

const fileFilter = (req, file, cb) => {
    if (!file.originalname.match(/\.(png|jpg|jpeg|gif|svg|pdf|mp4|mov|avi)$/)) {
        req.fileValidationError = 'File is not correct format !!!';
        return cb(new Error('File is not correct format !!!'), false);
    }
    cb(null, true);
}

const uploadMiddleware = multer({
    storage: storage,
    fileFilter: fileFilter
});

const uploadSingleCondition = uploadMiddleware.single('file') 
const uploadBlogCondition = uploadMiddleware.fields([{ name: 'image', maxCount: 1 }, { name: 'video', maxCount: 1 }])
const uploadMultipleCondition = uploadMiddleware.array('files', 10) 

function uploadSingle(req,res,next){
    uploadSingleCondition( req, res, next, function (err) {
        if (req.fileValidationError)
            return res.send({ code: 415, name: 'Unsupported Media Type !!!' });
        if (!req.file)
            return res.send('Please select an file to upload !!!');
        next()
    })
}

function uploadFileBlog(req,res,next){
    uploadBlogCondition( req, res, next, function (err) {
        if (req.fileValidationError) 
            return res.send({code: 415, name: 'Unsupported Media Type !!!'});
        if (!req.files) 
            return res.send('Please select an file to upload !!!');
        next()
    })
}

function uploadMultiple(req, res, next) {
    uploadMultipleCondition( req, res, next, function (err) {
        if (err instanceof multer.MulterError && err.code === "LIMIT_UNEXPECTED_FILE")
            return res.send({ code: 415, name: 'Unsupported Media Type !!!' });
        if (err) return res.json(err)
        if (!req.files) return res.send('Please select an file to upload !!!');
        next()
    })
}


module.exports = {
    uploadSingle,uploadMultiple,uploadFileBlog
}