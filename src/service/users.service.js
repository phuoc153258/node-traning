const { readFile, writeFile, setTimeZone, configPathUpload } = require('../helper/helper')
const { v4: uuidv4 } = require('uuid');

let Users = readFile('./src/data/user.json')
let Blogs = readFile('./src/data/blog.json')


const getUser = (req) => {
    let newUsers = [ ...Users ]
    let { search_value, field, type, limit, page } = req.query
    if(search_value) newUsers = searchUser(newUsers, search_value)
    if(field && type) newUsers = sortUser(newUsers, type, field)
    if(limit && page) newUsers = paginateUser(newUsers, limit, page)
    return newUsers
}

const getDetailsUser = (req) => {
    return Users.find(user => user.id === req.params.id)
}

const createUser = (req) => {
    const user = {
        ...req.body,
        avatar: configPathUpload(req.file.path),
        createdAt: setTimeZone(new Date(), 0),
        updatedAt: setTimeZone(new Date(), 0),
        id: uuidv4()
    }
    Users.push(user)
    writeFile('./src/data/user.json', JSON.stringify(Users))
    return user
}

const updateUser = (req) => {
    let { name, age, email } = req.body
    let user = Users.find(user => user.id === req.params.id)
    if(!user) return 'Users is not exits !!!'
    user.name = name
    user.age = age
    user.email = email
    user.avatar = configPathUpload(req.file.path)
    user.updatedAt = setTimeZone(new Date(), 0)
    writeFile('./src/data/user.json', JSON.stringify(Users))
    return user
}

const deleteUser = (req) => {
    let { id } = req.params
    let user = Users.find(user => user.id === id)
    if(!user) return 'Users is not exits !!!'
    Users = Users.filter(user => user.id != id)
    Blogs = Blogs.filter(blog => blog.authorId != id)
    writeFile('./src/data/user.json', JSON.stringify(Users))
    writeFile('./src/data/blog.json', JSON.stringify(Blogs))
    return user
}

const searchUser = (arr, search_value) => {
    let regex = new RegExp(`${search_value.toLowerCase()}`,'gi')
    let users = arr.filter( user => regex.test(user.name.toLowerCase()) || regex.test(user.email.toLowerCase()))
    return users
}

const sortUser = (arr, type, field) => {
    // let blogs = data.Users.sort((a,b) => ( req.query.field == 'age' 
    // ? ( req.query.type == 'desc' ? ( a.age == b.age ?  new Date(a.createdAt) - new Date(b.createdAt) : a.age - b.age) : b.age - a.age)
    // : ( req.query.type == 'desc' ?  ( new Date(a.createdAt) == new Date(b.createdAt) ? a.age - b.age :  new Date(b.createdAt) - new Date(a.createdAt) )  :  new Date(b.createdAt) - new Date(a.createdAt))))

    if( type == 'asc') arr.sort( (a,b) => field === 'age' ? a.age - b.age :  new Date(a.createdAt) - new Date(b.createdAt) )
    else arr.sort( (a,b) => field === 'age' ? b.age - a.age : new Date(b.createdAt) - new Date(a.createdAt) )
    return arr
}

const paginateUser = (arr, limit, page) => {
    let total_page = Math.ceil( arr.length / limit )
    let start = ( page - 1 ) * limit
    let end = start + limit
    const result = {
        "users": arr.slice(start, end),
        "paginate": {
            "limit": limit,
            "total_page": total_page,
            "page": page,
            "total_record": arr.length
        }
    }
    return result
}

module.exports = { getUser, createUser, updateUser, deleteUser, getDetailsUser }