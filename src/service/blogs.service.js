const { readFile, writeFile, setTimeZone, configPathUpload } = require('../helper/helper')
const { v4: uuidv4 } = require('uuid');
let Blogs = readFile('./src/data/blog.json')
let Users = readFile('./src/data/user.json')

const getBlogByAuthorId = (req) => {
    let newBlogs = [ ...Blogs ]
    let { authorId, search_value, field, type, limit, page } = req.query
    if(authorId){
        let user = Users.find(user => user.id === authorId)
        if(!user) return 'AuthorId is not exists !!!'
        newBlogs = newBlogs.filter(blog => blog.authorId === authorId)
    }
    if(search_value) newBlogs = searchBlog(newBlogs, search_value)
    if(field && type) newBlogs = sortBlog(newBlogs, type, field)
    if(limit && page) newBlogs = paginateBlog(newBlogs, limit, page)
    return newBlogs
}

const getDetailsBlog = (req) => {
    let { blogId, userId } = req.params
    if(!Users.find(user => user.id === userId)) return 'AuthorID is not exist !!!'
    let blog = Blogs.find(blog => blog.id === blogId && blog.authorId === userId)
    if(!blog) return 'Blogs is not exist !!!'
    return blog
}

const createBlog = (req) => {
    let { id } = req.params
    let { title, subTitle, content, isPublic } = req.body
    if(!Users.find(user => user.id === id)) return 'AuthorID is not exist !!!'
    const blog = {
        title,subTitle,content,isPublic,
        image: configPathUpload(req.files.image[0].path),
        video: configPathUpload(req.files.video[0].path),
        authorId: id,
        id: uuidv4(),
        count_view: 0,
        createdAt: setTimeZone(new Date(), 0),
        updatedAt: setTimeZone(new Date(), 0)
    }
    Blogs.push(blog) 
    writeFile('./src/data/blog.json',JSON.stringify(Blogs))
    return blog
}
const updateBlog = (req) => {
    let { blogId, userId } = req.params
    let { title, subTitle, content, isPublic, count_view } = req.body
    if(!Users.find(user => user.id === userId)) return 'AuthorID is not exist !!!'
    let blog = Blogs.find(blog => blog.id === blogId && blog.authorId === userId)
    if(!blog) return 'Blogs is not exist !!!'
    blog.title = title
    blog.subTitle = subTitle
    blog.content = content
    blog.isPublic = isPublic
    blog.count_view = count_view
    blog.updatedAt = setTimeZone(new Date(), 0)
    blog.image = configPathUpload(req.files.image[0].path)
    blog.video = configPathUpload(req.files.video[0].path)
    writeFile('./src/data/blog.json',JSON.stringify(Blogs))
    return blog
}
const deleteBlog = (req) => {
    let { blogId ,userId } = req.params
    if(!Users.find(user => user.id === userId)) return 'AuthorID is not exist !!!'
    let blog = Blogs.find(blog => blog.id === blogId && blog.authorId === userId)
    if(!blog) return 'Blogs is not exist !!!'
    Blogs = Blogs.filter( blog => blog.id != blogId || blog.authorId != userId)
    writeFile('./src/data/blog.json',JSON.stringify(Blogs))
    return blog
}

const paginateBlog = (arr, limit, page) => {
    let total_page = Math.ceil( arr.length / limit )
    let start = ( page - 1 ) * limit
    let end = start + limit
    const result = {
        "blogs": arr.slice(start, end),
        "paginate": {
            "limit": limit,
            "total_page": total_page,
            "page": page,
            "total_record": arr.length
        }
    }
    return result
}

const searchBlog = (arr, search_value) => {
    let regex = new RegExp(`${search_value.toLowerCase()}`,'gi')
    let blogs = arr.filter( blog => regex.test(blog.title.toLowerCase()) || regex.test(blog.subTitle.toLowerCase()) || regex.test(blog.content.toLowerCase()))
    return blogs
}

const sortBlog = (arr, type, field) => {
    if(type == 'asc') return arr.sort( (a,b) => field == 'count_view' ? a.count_view - b.count_view :  new Date(a.createdAt) - new Date(b.createdAt) )
    return arr.sort( (a,b) => field == 'count_view' ? b.count_view - a.count_view : new Date(b.createdAt) - new Date(a.createdAt) )
}

module.exports = { getBlogByAuthorId, createBlog, updateBlog, deleteBlog, getDetailsBlog }